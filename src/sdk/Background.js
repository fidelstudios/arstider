/**
 * Background
 * 
 * @version 1.1
 * @author frederic charette <fredericcharette@gmail.com>
 */
define( "Arstider/Background", ["Arstider/DisplayObject"], /** @lends Background */ function (DisplayObject) {	
	
	/**
	 * Background layer module definition
	 * Static redraw layer
	 * @class Background
	 * @constructor
	 */
	var background = new DisplayObject();

	background.clear = function() {
		this.data = null;
		this.removeChildren();
		this._requestedAsset = null;
	}

	return background;
});	